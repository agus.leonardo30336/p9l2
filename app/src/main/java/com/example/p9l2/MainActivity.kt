package com.example.p9l2

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.ArrayList
import java.util.concurrent.Future

class MainActivity : AppCompatActivity() {
    private val songList = ArrayList<SongInfo>()
    private var myIntent: Intent? = null
    private var playing = false
    private var pause = false
    private var index = 0
    private lateinit var async: Future<Unit>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadSongs()
        myIntent = Intent(this, MyMediaPlayerService::class.java)
        myIntent!!.action = ACTION_CREATE
        myIntent!!.putExtra("song", songList[index].songUrl)
        startService(myIntent)

        initMedia()
    }

    private fun setProgressBar(reset: Boolean, play: Boolean) {
        if (reset) progressBar.progress = 0

        if (play) {
            async = doAsync {
                uiThread {
                    progressBar.max = songList[index].duration!!.toInt()
                }
                while (progressBar.progress <= progressBar.max) {
                    progressBar.progress += 1000
                    Thread.sleep(1000)
                }
            }
        } else {
            async.cancel(true)
        }

    }

    @SuppressLint("SetTextI18n")
    private fun initMedia() {

        namaLagu.text = "${songList[index].songName} ${songList[index].artistName}"
        durasi.text = convertDuration(songList[index].duration!!)

        play_media.setOnClickListener {
            if (!playing) {
                myIntent!!.action = ACTION_PLAY
                startService(myIntent)
                playing = true
                setProgressBar(true, true)
            }
        }

        stop_media.setOnClickListener {
            if (playing) {
                myIntent!!.action = ACTION_STOP
                startService(myIntent)
                playing = false
            }
            setProgressBar(true, false)
        }

        forward_media.setOnClickListener {
            if (playing) {
                myIntent!!.action = ACTION_FORWARD
                startService(myIntent)

                progressBar.progress += 2000
            }
        }

        rewind_media.setOnClickListener {
            if (playing) {
                myIntent!!.action = ACTION_REWIND
                startService(myIntent)

                progressBar.progress -= 2000
            }
        }

        next_media.setOnClickListener {
            if (index < songList.size - 1) index++
            namaLagu.text = "${songList[index].songName} ${songList[index].artistName}"
            durasi.text = convertDuration(songList[index].duration!!)
            stopService(myIntent)
            myIntent!!.action = ACTION_CREATE
            myIntent!!.putExtra("song", songList[index].songUrl)
            startService(myIntent)

            myIntent!!.action = ACTION_PLAY
            startService(myIntent)
            playing = true

            async.cancel(true)
            setProgressBar(true, true)
        }

        previous_media.setOnClickListener {
            if (index > 0) index--
            namaLagu.text = "${songList[index].songName} ${songList[index].artistName}"
            durasi.text = convertDuration(songList[index].duration!!)

            stopService(myIntent)
            myIntent!!.action = ACTION_CREATE
            myIntent!!.putExtra("song", songList[index].songUrl)
            startService(myIntent)

            myIntent!!.action = ACTION_PLAY
            startService(myIntent)
            playing = true

            async.cancel(true)
            setProgressBar(true, true)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun loadSongs() {
        checkUserPermission()
        val uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val selection = MediaStore.Audio.Media.IS_MUSIC + "!=0"
        val cursor = contentResolver.query(uri, null, selection, null, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    val name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE))
                    val artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                    val duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION))
                    val url = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))

                    val s = SongInfo(name, artist, url, duration)
                    songList.add(s)

                } while (cursor.moveToNext())
            }
            cursor.close()
        }
    }

    private fun checkUserPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 123)
                return
            }
        }
    }

    private fun convertDuration(duration: Long): String? {
        var out: String? = null
        val hours: Long
        try {
            hours = duration / 3600000
        } catch (e: Exception) {
            e.printStackTrace()
            return out
        }

        val remainingMinutes = (duration - hours * 3600000) / 60000
        var minutes = remainingMinutes.toString()
        if (minutes == "0") {
            minutes = "00"
        }
        val remainingSeconds = duration - hours * 3600000 - remainingMinutes * 60000
        var seconds = remainingSeconds.toString()
        seconds = if (seconds.length < 2) {
            "00"
        } else {
            seconds.substring(0, 2)
        }

        out = if (hours > 0) {
            hours.toString() + ":" + minutes + ":" + seconds
        } else {
            "$minutes:$seconds"
        }

        return out
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            123 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadSongs()
            } else {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                checkUserPermission()
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        stopService(myIntent)
    }
}



