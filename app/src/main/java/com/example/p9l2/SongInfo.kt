package com.example.p9l2

class SongInfo(songName: String, artistName: String, songUrl: String, duration: Long) {
    var songName: String? = songName
    var artistName: String? = artistName
    var songUrl: String? = songUrl
    var duration: Long? = duration
}
