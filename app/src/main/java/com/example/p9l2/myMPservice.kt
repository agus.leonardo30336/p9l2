package com.example.p9l2

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.widget.Toast
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

const val ACTION_PLAY = "PLAY"
const val ACTION_PAUSE = "PAUSE"
const val ACTION_STOP = "STOP"
const val ACTION_FORWARD = "FORWARD"
const val ACTION_REWIND = "REWIND"
const val ACTION_CREATE = "CREATE"

class MyMediaPlayerService : Service(),
    MediaPlayer.OnPreparedListener,
    MediaPlayer.OnErrorListener,
    MediaPlayer.OnCompletionListener {

    private var myMediaPlayer: MediaPlayer? = null
    private fun init(url: String) {
        myMediaPlayer = MediaPlayer()
        myMediaPlayer!!.setDataSource(url)
        myMediaPlayer!!.setOnPreparedListener(this)
        myMediaPlayer!!.setOnCompletionListener(this)
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(
        intent: Intent?,
        flags: Int, startId: Int
    ): Int {
        if (intent != null) {
            val actionIntent = intent.action
            when (actionIntent) {
                ACTION_CREATE -> init(intent.getStringExtra("song"))
                ACTION_PLAY -> {
                    if (!myMediaPlayer!!.isPlaying) {
                        myMediaPlayer!!.prepareAsync()
                    }
                }
                ACTION_STOP -> myMediaPlayer!!.stop()
                ACTION_FORWARD -> myMediaPlayer!!.seekTo(myMediaPlayer!!.currentPosition + 2000)
                ACTION_REWIND -> myMediaPlayer!!.seekTo(myMediaPlayer!!.currentPosition - 2000)
            }

        }
        return flags
    }

    override fun onError(p0: MediaPlayer?, p1: Int, p2: Int)
            : Boolean {
        return false
    }

    override fun onCompletion(p0: MediaPlayer?) {
        Toast.makeText(this, "Player Stop", Toast.LENGTH_SHORT).show()
    }

    override fun onPrepared(p0: MediaPlayer?) {
        myMediaPlayer!!.start()
    }

    override fun onDestroy() {
        if (myMediaPlayer != null) {
            myMediaPlayer!!.release()
        }
    }
}


